import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from "./components/Nav";
import Proveedores from "./Views/Proveedores/Index";
import CreateProveedores from "./Views/Proveedores/Create";
import EditProveedores from  "./Views/Proveedores/Edit";
import Productos from "./Views/Productos/index";
import Pedidos from "./Views/Pedidos/index";
import Categorias from "./Views/Categorias/index";
import Graphic from "./Views/Productos/Graphic";
import Login from "./Views/Login";
import Register from "./Views/Register";
import ProtectedRoutes from "./components/ProtectedRoutes";

function App(){

  return(
<BrowserRouter>
<Nav/>

<Routes>
<Route path="/login" element={<Login/>}/>
<Route path="/register" element={<Register/>}/>
<Route element={<ProtectedRoutes/>}>
<Route path="/edit/:id" element={<EditProveedores/>}/>
<Route path="/" element={<Proveedores/>}/>
<Route path="/create" element={<CreateProveedores/>}/>
<Route path="/productos" element={<Productos/>}/>
<Route path="/pedidos" element={<Pedidos/>}/>
<Route path="/categorias" element={<Categorias/>}/>
<Route path="/graphic" element={<Graphic/>}/>
</Route>
</Routes>


</BrowserRouter>
  )
}

export default App