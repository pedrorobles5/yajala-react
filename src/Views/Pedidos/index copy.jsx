import React, { useEffect,useRef,useState } from 'react';
import DivTable from '../../components/DivTable';
import DivSelect from '../../components/DivSelect';
import DivInput from '../../components/DivInput';
import Modal from '../../components/Modal';
import {confirmation, sendReequest} from '../../functions';
import { PaginationControl } from 'react-bootstrap-pagination-control'; 
import DivAdd from '../../components/DivAdd';

const Pedidos = () => {
  const [pedidos,setPedidos] =useState('');
 
  const [nomCliente,setnomCliente] =useState('');
  const [direccion,setDireccion] =useState('');
  const [telefono,setTelefono] =useState('');
  const [productoID,setProductoID] =useState('');
  const [ID,setID] =useState('');
 
  const [operation,setOperation] =useState('');
  const [title,setTitle] =useState('');
  const [classLoad,setClassLoad] =useState('');
  const [classTable,setClassTable] =useState('d-none');
  const [rows,setRows] =useState(0);
  const [page,setPage] =useState(1);
  const [pageSize,setPageSize] =useState(0);
  const NameInput =useRef();
  const close = useRef();
  let method = '';
  let url='';


    useEffect(()=>{
      getPedidos(1);
      getProducto();
    },[]);
      
    const getPedidos = async(page) =>{
      const res = await sendReequest('GET','','/api/pedidos?page=1'+page,'');
      setPedidos(res.data);
      setRows(res.total);
      setPageSize(res.per_page);
      setClassTable('');
      setClassLoad('d-node');
    }

    const getProducto = async()=>{
      const res = await sendReequest('GET','','/api/pedidos','');
      getProducto(res);
    }
    
    const deletePedidos =(id,name) =>{
      confirmation(name,'/api/pedidos/'+id,'pedidos');
  }

  const clear =()=>{

    setnomCliente('');
    setDireccion('');
    setTelefono('');
    setProductoID('');
  }

  const openModal = (op, cli, dire, pre, te, pid,k) => {
    clear();
    setTimeout(() => NameInput.current.focus(), 600);
    setOperation(op);
    setID(k);
    if (op === 1) {
      setTitle('pedidos creado');
    } else {
      setTitle('Actualados pedido');
      setnomCliente(cli);
      setDireccion(dire);
      setPrecio(pre);
      setTelefono(te);
      setProductoID(pid);
    }
  }
  

  const save = async(e) =>{
    e.preventDefault();
    if(operation == 1){
      method='POST';
      url = '/api/pedidos';
  
    }
    else{
      method = 'PUT';
      url = '/api/pedidos/'+ID;
  
    }
        const form  ={nomCliente:nomCliente,direccion:direccion,telefono:telefono,producto_id:productoID}
        const res = await sendReequest(method,form,url,'');
        if(method == 'PUT' && res.status == true){
          close.current.click();
        }
        if(res.status == true){
          clear();
          getPedidos(page);
          setTimeout( ()=>NameInput.current.focus(),3000);
        }
      }
      const goPage = (p)=>{
        setPage(p);
        getPedidos(p);
      }

      return(

        <div className='container-fluid'>
        <DivAdd>
            
          <button className='btn btn-dark' data-bs-toggle='modal'
    data-bs-target='#modalEmployees' onClick={()=>openModal(1)}>
      <i className='fa-solid fa-circle-plus'></i>Add
    </button>

          </DivAdd>
          <DivTable col='14' off='3' classLoad={classLoad} classTable={classTable}>
            <table className='table-group-bordered'>
                <thead><tr><th>#</th><th>Nombre</th>
                <th>Precio</th>
                <th>Categoria</th>
                <th>Proveedor</th>
                <th></th>
                <th></th>
                </tr></thead>
                  <tbody className='table-group-divider'>
                    {productos.map((row,i)=>(
                       <tr key={row.id}>
                        <td>{(i+1)}</td>
                        <td>{(row.nomCliente)}</td>
                        <td>{(row.direccion)}</td>
                        <td>{(row.telefono)}</td>
                        <td>{(row.producto_id)}</td>
                        <td>
                        <button className='btn btn-dark' data-bs-toggle='modal'
      data-bs-target='#modalEmployees'
      
      onClick={()=>openModal(2,row.nomCliente,row.direccion,row.telefono,row.producto_id)}>
        <i className='fa-solid fa-circle-plus'></i>Edit
      </button>
                          </td>
                          <td>
                            <button className='btn btn-danger'
                            onClick={()=> deletePedidos(row.id,row.nomProducto)}>
                              <i className='fa-solid fa-trash'></i>
                            </button>
                          </td>
                        </tr>
                      ))};
                         </tbody>
                         </table>
                <PaginationControl changePage={page => goPage(page)}
                 next={true} limit={pageSize} page={page} total={rows} />
               
                </DivTable>

                <Modal tittle={title} modal='modalEmployees'>
                <div className='modal-body'>
                  <form onSubmit={save}>

                  <DivInput type='text' icon='fa-user' 
                  value={nomCliente} className='form-control'
                  placeholder='Name'  required='required' ref={NameInput}
                  handleChange={(e)=>setnomCliente(e.target.value)}/>
                 
                 <DivInput type='text' icon='fa-at' 
                  value={telefono} className='form-control'
                  placeholder='Precio' required='required' 
                  handleChange={(e)=>setTelefono(e.target.value)}/>

<DivInput type='text' icon='fa-at' 
                  value={direccion} className='form-control'
                  placeholder='Departmento id' required='required' 
                  handleChange={(e)=>setDireccion(e.target.value)}/>

<DivInput type='text' icon='fa-at' 
                  value={productoID} className='form-control'
                  placeholder='' required='required' 
                  handleChange={(e)=>setProductoID(e.target.value)}/>

                 
                  <div className='d-grid col-10 mx-auto'>
                                        <button className='btn btn-success'>
                                            <i className='fa-solid fa-save'></i>Save
                                        </button>
                                        </div>
                   </form>
                  </div>
                  <div className='modal-footer'>
                    <button className='btn btn-dark' data-bs-dismiss='modal'
                    ref={close}> Close
                    </button>
                  </div>
               </Modal>
             </div>
                     
    )
    }


export default Pedidos
