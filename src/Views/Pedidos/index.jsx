import React, { useEffect,useRef,useState } from 'react';
import DivTable from '../../components/DivTable';
import DivSelect from '../../components/DivSelect';
import DivInput from '../../components/DivInput';
import Modal from '../../components/Modal';
import {confirmation, sendReequest} from '../../functions';
import { PaginationControl } from 'react-bootstrap-pagination-control'; 
import DivAdd from '../../components/DivAdd';

const Productos = () => {
  const [productos,setProductos] =useState([]);
  const [proveedores,setProveedores] =useState([]);
  const [id,setId] =useState('');
  const [nomCliente,setnomCliente] =useState('');
  const [producto_id,Setproducto_id] =useState([]);
  const [telefono,setTelefono] =useState([]);
  const [direccion,SetDireccion] =useState('');
  const [operation,setOperation] =useState('');
  const [title,setTitle] =useState('');
  const [classLoad,setLoad] = useState('');
  const [classTable,setClassTable] = useState('d-none');
  const [rows,setRows] =useState(0);
  const [page,setPage] =useState(1);
  const [pageSize,setPageSize] =useState(0);
  const NameInput =useRef();
  const close = useRef();
  let method = '';
  let url='';

    useEffect(()=>{
      getProductos(1);
      getProveedores();
    },[]);
    const getProductos = async(page) =>{
      const res = await sendReequest('GET','','/api/pedidos?page='+page,'');
      setProductos(res.data);
      setRows(res.total);
      setPageSize(res.per_page);
      setClassTable('');
      setLoad('d-none');
    }

    const getProveedores = async()=>{
      const res = await sendReequest('GET','','/api/pedidos','');
      setProveedores(res);
    }

    const deleteProductos =(id,name) =>{
        confirmation(name,'/api/pedidos/'+id,'productos');
    }

    const clear =()=>{

      setnomCliente('');
      SetDireccion('');
      setTelefono('');
      Setproducto_id('');
    }
    const openModal = (op, pren, ta, pre, de, mj) => {
      clear();
      setTimeout(() => NameInput.current.focus(), 600);
      setOperation(op);
      setId(mj);
      if (op === 1) {
        setTitle('Crear Pedido');
      } else {
        setTitle('Pedido Actualizado');
        setnomCliente(pren);
        SetDireccion(ta);
        setTelefono(pre);
        Setproducto_id(de);
        
      }


    }

    const save = async(e) =>{
      e.preventDefault();
      if(operation == 1){
        method='POST';
        url = '/api/pedidos';
    
      }
      else{
        method = 'PUT';
        url = '/api/pedidos/'+id;
    
      }

      const form = {nomCliente:nomCliente,direccion:direccion,telefono:telefono,producto_id:producto_id};
      const res = await sendReequest(method,form,url,'');
      if(method == 'PUT' && res.status == true){
        close.current.click();
    }
    if(res.status == true){
      clear();
      getProductos(page);
      setTimeout( () => NameInput.current.focus(),3000);  
    }
  }
    
      const goPage =(p) =>{
        setPage(p);
        getProductos(p);
      }

      
    
      return(
      
          <div className='container-fluid'>
          <DivAdd>
              
            <button className='btn btn-dark' data-bs-toggle='modal'
      data-bs-target='#modalEmployees' onClick={()=>openModal(1)}>
        <i className='fa-solid fa-circle-plus'></i>Add
      </button>

            </DivAdd>
            <DivTable col='14' off='3'  classLoad={classLoad}  classTable={classTable}>
              <table className='table-group-bordered'>
                  <thead><tr><th>#</th>
                  <th>Nombre</th>
                  <th>Direccion</th>
                  <th>telefono</th>
                  <th>Produtc ID</th>
                  <th></th>
                  <th></th>
                  </tr></thead>
                    <tbody className='table-group-divider'>
                      {productos.map((row,i)=>(
                         <tr key={row.id}>
                          <td>{(i+1)}</td>
                          <td>{(row.nomCliente)}</td>
                          <td>{(row.direccion)}</td>
                          <td>{(row.telefono)}</td>
                          <td>{(row.producto_id)}</td>
                          <td>
                          <button className='btn btn-dark' data-bs-toggle='modal'
      data-bs-target='#modalEmployees'
      
      onClick={()=>openModal(2,row.nomProducto,row.precio,row.categoria_id,row.proveedor_id)}>
        <i className='fa-solid fa-circle-plus'></i>Edit
      </button>
                          </td>
                          <td>
                            <button className='btn btn-danger'
                            onClick={()=> deleteProductos(row.id,row.nomProducto)}>
                              <i className='fa-solid fa-trash'></i>
                            </button>
                          </td>
                        </tr>
                      ))};
                         </tbody>
                         </table>
                <PaginationControl changePage={page => goPage(page)}
                 next={true} limit={pageSize} page={page} total={rows} />
               
                </DivTable>

                <Modal tittle={title} modal='modalEmployees'>
                <div className='modal-body'>
                  <form onSubmit={save}>

                  <DivInput type='text' icon='fa-user' 
                  value={nomCliente} className='form-control'
                  placeholder='Nombre'  required='required' ref={NameInput}
                  handleChange={(e)=>setnomCliente(e.target.value)}/>
                 
                 <DivInput type='text' icon='fa-house' 
                  value={direccion} className='form-control'
                  placeholder='Dirrecion' required='required' 
                  handleChange={(e)=>SetDireccion(e.target.value)}/>

                  <DivInput type='text' icon='fa-phone' 
                  value={telefono} className='form-control'
                  placeholder='Telefono' required='required' 
                  handleChange={(e)=>setTelefono(e.target.value)}/>

                  <DivInput type='text' icon='fa-store' 
                  value={producto_id} className='form-control'
                  placeholder='Producto ID' required='required' 
                  handleChange={(e)=>Setproducto_id(e.target.value)}/>

                 
                  <div className='d-grid col-10 mx-auto'>
                                        <button className='btn btn-success'>
                                            <i className='fa-solid fa-save'></i>Save
                                        </button>
                                        </div>
                   </form>
                  </div>
                  <div className='modal-footer'>
                    <button className='btn btn-dark' data-bs-dismiss='modal'
                    ref={close}> Close
                    </button>
                  </div>
               </Modal>
             </div>
                     
    )
    }

export default Productos
