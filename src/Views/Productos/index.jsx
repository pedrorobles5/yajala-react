import React, { useEffect,useRef,useState } from 'react';
import DivTable from '../../components/DivTable';
import DivSelect from '../../components/DivSelect';
import DivInput from '../../components/DivInput';
import Modal from '../../components/Modal';
import {confirmation, sendReequest} from '../../functions';
import { PaginationControl } from 'react-bootstrap-pagination-control'; 
import DivAdd from '../../components/DivAdd';

const Productos = () => {
  const [productos,setProductos] =useState([]);
  const [proveedores,setProveedores] =useState([]);
  const [id,setId] =useState('');
  const [nomproducto,setnomProducto] =useState('');
  const [cateid,setCateId] =useState([]);
  const [proveedorid,setProveedorId] =useState([]);
  const [precio,setPrecio] =useState('');
  const [operation,setOperation] =useState('');
  const [title,setTitle] =useState('');
  const [classLoad,setLoad] = useState('');
  const [classTable,setClassTable] = useState('d-none');
  const [rows,setRows] =useState(0);
  const [page,setPage] =useState(1);
  const [pageSize,setPageSize] =useState(0);
  const NameInput =useRef();
  const close = useRef();
  let method = '';
  let url='';

    useEffect(()=>{
      getProductos(1);
      getProveedores();
    },[]);
    const getProductos = async(page) =>{
      const res = await sendReequest('GET','','/api/productos?page='+page,'');
      setProductos(res.data);
      setRows(res.total);
      setPageSize(res.per_page);
      setClassTable('');
      setLoad('d-none');
    }

    const getProveedores = async()=>{
      const res = await sendReequest('GET','','/api/productos','');
      setProveedores(res);
    }

    const deleteProductos =(id,name) =>{
        confirmation(name,'/api/productos/'+id,'productos');
    }

    const clear =()=>{

      setnomProducto('');
      setPrecio('');
      setProveedorId('');
      setCateId('');
    }

    const openModal = (op, pren, ta, pre, de, mj) => {
      clear();
      setTimeout(() => NameInput.current.focus(), 600);
      setOperation(op);
      setId(mj);
      if (op === 1) {
        setTitle('Crear Producto');
      } else {
        setTitle('Producto Actualizado');
        setnomProducto(pren);
        setPrecio(ta);
        setProveedorId(pre);
        setCateId(de);
        
      }


    }

    const save = async(e) =>{
      e.preventDefault();
      if(operation == 1){
        method='POST';
        url = '/api/productos';
    
      }
      else{
        method = 'PUT';
        url = '/api/productos/'+id;
    
      }

      const form = {nomProducto:nomproducto,precio:precio,categoria_id:cateid,proveedor_id:proveedorid};
      const res = await sendReequest(method,form,url,'');
      if(method == 'PUT' && res.status == true){
        close.current.click();
    }
    if(res.status == true){
      clear();
      getProductos(page);
      setTimeout( () => NameInput.current.focus(),3000);  
    }
  }
    
      const goPage =(p) =>{
        setPage(p);
        getProductos(p);
      }

      
    
      return(
      
          <div className='container-fluid'>
          <DivAdd>
              
            <button className='btn btn-dark' data-bs-toggle='modal'
      data-bs-target='#modalEmployees' onClick={()=>openModal(1)}>
        <i className='fa-solid fa-circle-plus'></i>Add
      </button>

            </DivAdd>
            <DivTable col='14' off='3' classLoad={classLoad} classTable={classTable}>
              <table className='table-group-bordered'>
                  <thead><tr><th>#</th><th>Nombre</th>
                  <th>Precio</th>
                  <th>Categoria</th>
                  <th>Proveedor</th>
                  <th></th>
                  <th></th>
                  </tr></thead>
                    <tbody className='table-group-divider'>
                      {productos.map((row,i)=>(
                         <tr key={row.id}>
                          <td>{(i+1)}</td>
                          <td>{(row.nomProducto)}</td>
                          <td>{(row.precio)}</td>
                          <td>{(row.categoria_id)}</td>
                          <td>{(row.proveedor_id)}</td>
                          <td>
                          <button className='btn btn-dark' data-bs-toggle='modal'
      data-bs-target='#modalEmployees'
      
      onClick={()=>openModal(2,row.nomProducto,row.precio,row.categoria_id,row.proveedor_id)}>
        <i className='fa-solid fa-circle-plus'></i>Edit
      </button>
                          </td>
                          <td>
                            <button className='btn btn-danger'
                            onClick={()=> deleteProductos(row.id,row.nomProducto)}>
                              <i className='fa-solid fa-trash'></i>
                            </button>
                          </td>
                        </tr>
                      ))};
                         </tbody>
                         </table>
                <PaginationControl changePage={page => goPage(page)}
                 next={true} limit={pageSize} page={page} total={rows} />
               
                </DivTable>

                <Modal tittle={title} modal='modalEmployees'>
                <div className='modal-body'>
                  <form onSubmit={save}>

                  <DivInput type='text' icon='fa-user' 
                  value={nomproducto} className='form-control'
                  placeholder='Name'  required='required' ref={NameInput}
                  handleChange={(e)=>setnomProducto(e.target.value)}/>
                 
                 <DivInput type='text' icon='fa-at' 
                  value={precio} className='form-control'
                  placeholder='Precio' required='required' 
                  handleChange={(e)=>setPrecio(e.target.value)}/>

<DivInput type='text' icon='fa-at' 
                  value={proveedorid} className='form-control'
                  placeholder='Departmento id' required='required' 
                  handleChange={(e)=>setCateId(e.target.value)}/>

<DivInput type='text' icon='fa-at' 
                  value={cateid} className='form-control'
                  placeholder='' required='required' 
                  handleChange={(e)=>setProveedorId(e.target.value)}/>

                 
                  <div className='d-grid col-10 mx-auto'>
                                        <button className='btn btn-success'>
                                            <i className='fa-solid fa-save'></i>Save
                                        </button>
                                        </div>
                   </form>
                  </div>
                  <div className='modal-footer'>
                    <button className='btn btn-dark' data-bs-dismiss='modal'
                    ref={close}> Close
                    </button>
                  </div>
               </Modal>
             </div>
                     
    )
    }

export default Productos
