import React from 'react';
import { useParams } from 'react-router-dom';
import FormDep from '../../components/FormDep';

const Edit = () => {
  const {id} = useParams();
  return(
    <FormDep id={id} title='Edit Proveedor'></FormDep>
  )
}

export default Edit