import React,{useEffect,useState}from 'react';
import DivAdd from '../../components/DivAdd';
import DivTable from '../../components/DivTable';
import { Link } from 'react-router-dom';
import { confirmation,sendReequest } from '../../functions';
const Proveedors= () => {
 
  const [proveedors,setProveedors]= useState([]);
  const [classLoad,setLoad] = useState('');
  const [classTable,setClassTable] = useState('d-none');
  useEffect( () =>{
    getProveedors();
  },[]);
 const getProveedors = async() =>{
  const res = await sendReequest('GET','','/api/proveedores','');
  setProveedors(res);
  setClassTable('');
  setLoad('d-none');
}
const deleteProveedors =(id,name) =>{
  confirmation(name,'/api/proveedores/'+id,'/');
}

  return (
    <div className='container-fluid'>
      <DivAdd>
        <Link to='create' className='btn btn-dark'>
          <i className='fa-solid fa-circle-plus'></i>Add
         </Link>
      </DivAdd>
                           
      <DivTable col='12' off='2' classLoad={classLoad} classTable={classTable}>
        <table className='table-group-bordered'>
            <thead><tr><th>#</th><th>Proveedor</th><th></th><th></th></tr></thead>
              <tbody className='table-group-divider'>
                {proveedors.map((row,i)=>(
                   <tr key={row.id}>
                    <td>{(i+1)}</td>
                    <td>{(row.proveedor)}</td>
                    <td>
                      <Link to={'/edit/'+row.id} className='btn btn-warning'>
                        <i className='fa-solid fa-edit'></i>
                      </Link>
                    </td>
                    <td>
                      <button className='btn btn-danger'
                      onClick={()=> deleteProveedors(row.id,row.proveedor)}>
                        <i className='fa-solid fa-trash'></i>
                      </button>
                    </td>
                  </tr>
                ))};
                   </tbody>
                   </table>
                 </DivTable>
               </div>
            
  )
}

export default Proveedors
