import React,{useEffect,useState,useRef} from 'react';
import {sendReequest} from '../functions';
import DivInput from './DivInput';

const FormDep = (params) =>{
        const [proveedor,setProveedor] =useState('');
        const ProveedorInput = useRef();
        let method = 'POST';
        let url = '/api/proveedores';
        let redirect = '';
        useEffect (() => {

            ProveedorInput.current.focus();
            getProveedores();
        },[]);
        const getProveedores = async()=>{
            if(params.id !== null){

                const res = await sendReequest('GET','',(url+'/'+params.id));
                setProveedor(res.data.proveedor);
            }
}
        const save = async(e) =>{
            e.preventDefault();
                if(params.id !==null){
                    method= 'PUT';
                    url = '/api/proveedores/'+params.id;
                    redirect ='/'
                }
                const res= await sendReequest(method,{proveedor:proveedor},url,redirect);
                if(method =='POST' && res.status == true){
                    setProveedor('');
                }
        }
    return(
            <div className='container-fluid'>
                <div className='row mt-5'>
                    <div className='col-md-4 offset-md-4'>
                        <div className='card border border-info'>
                             <div className='card-header bg-info  border border-info'>
                             {params.tittle}
                            
                        </div>
                        <div className='card-body'>
                            <form onSubmit={save}>
                                <DivInput type='text' icon='fa-building' 
                                value={proveedor} className='form-control'
                                placeholder='Proveedr' required='required'
                                ref={ProveedorInput}
                                handleChange={(e)=>setProveedor(e.target.value)}/>
                                    <div className='d-grid col-10 mx-auto'>
                                        <button className='btn btn-dark'>
                                            <i className='fa-solid fa-save'></i>Save
                                        </button>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>




    )
}

export default FormDep