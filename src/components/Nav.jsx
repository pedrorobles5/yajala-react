import { Link, useNavigate } from 'react-router-dom';
import storage from '../Storage/storage';


const Nav =() =>{
  const go = useNavigate();
  const logout = async()=>{
    storage.remove('authToken');
    storage.remove('authUser');
    await axios.get('/api/auth/logout',storage.get('authToken'));
    go('/login');
  }
return (


  <nav className='navbar navbar-expand-lg navbar-white bg-danger'>
   

  
       <img
  src="https://dynl.mktgcdn.com/p/yROFyV7n-wDcC7wnlxpmzSNDbSidG9tujRWg3qF3hp4/950x950.png"
  
  width="10%" height="10%" marning="auto"/>
  
      <button className='navbar-toggler' type='button' data-bs-toggle='collapse'
        data-bs-target='#nav' aria-controls='navbarSupportedContent'>
          <span className='navbar-toggler-icon'></span>
</button>



{storage.get('authUser') ?(
  <div className='collapse navbar-collapse' id='nav'>
    <ul className='navbar-nav mx-auto mb-2'>
      <li className='nav-item px-lg-5 h4 bg-warning icon-user '>
      {storage.get('authUser').name }
      </li>
   
      <li><span class="fa-li">
      <i class="fa-solid fa-user bg-warning">
      </i></span>
      <Link to='/' className='nav-link'>Proveedores</Link>
      </li>

      <li><span class="fa-li">
      <i class="fa-brands fa-whatsapp bg-warning"></i>
</span>
      <Link to='/pedidos' className='nav-link'>Pedidos</Link>
      </li>
        
      <li><span class="fa-li">
      <i class="fa-solid fa-layer-group bg-warning"></i></span>
      <Link to='/categorias' className='nav-link'>Categorias</Link>
      </li>

     
      
      <li><span class="fa-li">
      <i class="fa-solid fa-people-carry-box bg-warning"></i></span>
      <Link to='/productos' className='nav-link'>Productos</Link>
      </li>
      
      <li><span class="fa-li">
      <i class="fa-solid fa-chart-line bg-warning"></i>
</span>
      <Link to='/graphic' className='nav-link'>Grafica</Link>
      </li>
        
    </ul>
    <ul className='navbar-nav mx-auto mb-2 bg-warning'>
        <li className='nav-item px-lg-5'> <i class=""></i>

            <button className='btn btn-info ' onClick={logout}>
            <i class="fa-solid fa-right-from-bracket"></i>Logout</button>
        </li>
  </ul>
</div>


) : ''}

  </nav>
 
)
}

export default Nav